package university.block;

import java.util.UUID;

public class DiagMatrix implements IMatrix {
    private double matrix[];
    int size;
    private UUID uuid;
    private UUID uuidDefault;
    SolveMatrix solveMatrix = new SolveMatrix();
    DiagMatrix(int size) throws MatrixException {
        uuid = UUID.randomUUID();
        if (size <= 0) {
            throw new MatrixException("error size");
        }
        this.size = size;
        matrix = new double[(size * size)];
        for (int i = 0; i < size; i++) {
            matrix[i] = 0;
        }
    }

    DiagMatrix(double[] matrix) throws MatrixException {
        uuid = UUID.randomUUID();
        this.size = (int) Math.sqrt(matrix.length);
        this.matrix = matrix;
        for (int i = 0; i < Math.sqrt(size); i++) {
            for (int j = 0; j < Math.sqrt(size); j++) {
                if (i != j && matrix[i * size + j] != 0) {
                    throw new MatrixException("not zero on diagonal");
                }
            }
        }
    }


    @Override
    public double getElement(int x, int y) {
        return matrix[x*size+y];
    }

    @Override
    public void setElement(int x, int y, double value) throws MatrixException {
        if (x != y && value != 0) {
            throw new MatrixException("not zero on diagonal");
        }
        matrix[x *size+ y] = value;
        uuid = UUID.randomUUID();

    }
    @Override
    public double solveMatrix() {
        if (uuid != uuidDefault) {
            uuidDefault = uuid;
            return solveMatrix.setParamsAndSolve(matrix, size*size, 1);

        } else {
            return solveMatrix.setParamsAndGetResult(matrix, size*size, 1);
        }
    }

}
