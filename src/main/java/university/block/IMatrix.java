package university.block;

public interface IMatrix {
    double getElement(int x, int y);

    void setElement(int x, int y, double value) throws MatrixException;

    double solveMatrix();

}
