package university.block;

import java.util.UUID;

public class SolveMatrix {


    SolveMatrix() {

    }

    public double solve(double[] matrix, int size, int minus) {
        double stroke = Math.sqrt(size);
        double[] massFinish = matrix;
        double initial;
        double result;


        for (int i = 0; i < stroke - 1; i++) {

            for (int j = i + 1; j < stroke; j++) {
                initial = massFinish[(int) (stroke * j + i)] / massFinish[(int) (stroke * i + i)];
                for (int k = i; k < stroke; k++) {
                    massFinish[(int) (stroke * j + k)] = massFinish[(int) (stroke * i + k)] * initial - massFinish[(int) (stroke * j + k)];
                }
            }

        }


        result = (checkMinus(minus)) * massFinish[(int) (0)];
        for (int i = 1; i < stroke; i++) {
            result *= massFinish[(int) (stroke * i + i)];

        }
        return result;
    }

    private int checkMinus(int minus) {
        if (minus % 2 == 0) {
            minus = 1;
        } else {
            minus = -1;
        }
        return minus;
    }

    public double getResult(int minus,  double[] massFinish,double stroke) {
        double result;
        result = (checkMinus(minus)) * massFinish[(int) (0)];
        for (int i = 1; i < stroke; i++) {
            result *= massFinish[(int) (stroke * i + i)];

        }
        return result;
    }

    public double setParamsAndSolve(double[] matrix, int size, int minus) {
        double  stroke = Math.sqrt(size);
        double[]   massFinish = matrix;
        double initial;
        for (int i = 0; i < stroke - 1; i++) {

            for (int j = i + 1; j < stroke; j++) {
                initial = massFinish[(int) (stroke * j + i)] / massFinish[(int) (stroke * i + i)];
                for (int k = i; k < stroke; k++) {
                    massFinish[(int) (stroke * j + k)] = massFinish[(int) (stroke * i + k)] * initial - massFinish[(int) (stroke * j + k)];
                }
            }

        }
        return getResult(minus,massFinish,stroke);
    }

    public double setParamsAndGetResult(double[] matrix, int size, int minus) {
        double  stroke = Math.sqrt(size);
        double[]   massFinish = matrix;
        double initial;
        return getResult(minus,massFinish,stroke);
    }
}

