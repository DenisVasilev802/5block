package university.block;

import java.util.Objects;
import java.util.UUID;

public class SquareMatrix implements IMatrix {
    private double matrix[];
    private UUID uuid;
    private UUID uuidDefault;
    private int determinant = 0;
    private int size = 0;
    int changeMinus = 1;
    SolveMatrix solveMatrix = new SolveMatrix();

    SquareMatrix(int size) throws MatrixException {
        if (size <= 0) {
            throw new MatrixException("error size");
        }
        uuid = UUID.randomUUID();
        matrix = new double[size * size];
        this.size = size * size;
        for (int i = 0; i < size; i++) {
            matrix[i] = 0;
        }
    }

    SquareMatrix(double[] matrix) {
        uuid = UUID.randomUUID();
        size = matrix.length;
        double[] tmrMatrix = matrix;
        for (int i = 0; i < Math.sqrt(size); i++) {
            for (int j = 0; j < Math.sqrt(size); j++) {
                if (i == j && matrix[(int) (i * Math.sqrt(size) + j)] == 0) {
                    tmrMatrix = swapStrokes(i, j, matrix);
                }
            }
        }
        this.matrix = tmrMatrix;
    }

    @Override
    public double getElement(int x, int y) {
        return matrix[(int) (x * Math.sqrt(size) + y)];
    }

    @Override
    public void setElement(int x, int y, double value) {
        matrix[(int) (x * Math.sqrt(size) + y)] = value;
        uuid = UUID.randomUUID();
        for (int i = 0; i < Math.sqrt(size); i++) {
            for (int j = 0; j < Math.sqrt(size); j++) {
                if (i == j && matrix[(int) (i * Math.sqrt(size) + j)] == 0) {
                    matrix = swapStrokes(i, j, matrix);
                }
            }
        }

    }

    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        if (!super.equals(object)) return false;
        SquareMatrix that = (SquareMatrix) object;
        return java.util.Arrays.equals(matrix, that.matrix);
    }

    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + java.util.Arrays.hashCode(matrix);
        return result;
    }


    public double solveMatrix() {
        if (uuid != uuidDefault) {
            uuidDefault = uuid;
            return solveMatrix.setParamsAndSolve(matrix, size, changeMinus);

        } else {
            return solveMatrix.setParamsAndGetResult(matrix, size, changeMinus);
        }

    }

    private double[] swapStrokes(int i, int j, double[] matrix) {
        double tmpr;
        for (int k = 0; k < Math.sqrt(matrix.length) + 1; k++) {
            if (matrix[(int) (k * Math.sqrt(size) + j)] != 0) {
                changeMinus++;
                for (int l = 0; l < Math.sqrt(size); l++) {
                    tmpr = matrix[(int) (k * Math.sqrt(size) + l)];
                    matrix[(int) (k * Math.sqrt(size) + l)] = matrix[(int) (i * Math.sqrt(size) + l)];
                    matrix[(int) (i * Math.sqrt(size) + l)] = tmpr;
                }
                return matrix;
            }

        }
        return matrix;
    }

}
