package university.block;

import java.util.UUID;

public class UpTriangleMatrix implements IMatrix {
    double[] matrix;
    private UUID uuid;
    int size;
    private UUID uuidDefault;
    SolveMatrix solveMatrix = new SolveMatrix();

    UpTriangleMatrix(int size) throws MatrixException {
        uuid = UUID.randomUUID();
        if (size <= 0) {
            throw new MatrixException("error size");
        }
        matrix = new double[size * size];
        for (int i = 0; i < size; i++) {
            matrix[i] = 0;

        }
    }

    UpTriangleMatrix(double[] matrix) throws MatrixException {
        uuid = UUID.randomUUID();
        this.size = (int) Math.sqrt(matrix.length);
        int sizeM = 0;
        this.matrix = matrix;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if ((i != j || i < j) && matrix[i * size + j] != 0) {
                    throw new MatrixException("not zero under diagonal");
                }
            }
        }
    }

    @Override
    public double getElement(int x, int y) {
        return matrix[x*size+y];
    }

    @Override
    public void setElement(int x, int y, double value) throws MatrixException {
        if ((x != y || x < y) && value != 0) {
            throw new MatrixException("not zero under diagonal");
        }
        matrix[x *size+ y] = value;
        uuid = UUID.randomUUID();

    }

    public double solveMatrix() {
        if (uuid != uuidDefault) {
            uuidDefault = uuid;
            return solveMatrix.setParamsAndSolve(matrix, size*size, 1);

        } else {
            return solveMatrix.setParamsAndGetResult(matrix, size*size, 1);
        }

    }
}
