package university.block;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class DiagMatrixTest {
    IMatrix matrix;
    @Test
    public void testSimple() throws MatrixException {
        double[] matrixd = {1,0,0,0,4,0,0,0,8};
        double result =32;
       matrix = new DiagMatrix(matrixd);
        assertEquals(matrix.solveMatrix(), result);
    }
    @Test
    public void testSet() throws MatrixException {
        double[] matrixd = {1,0,0,0,4,0,0,0,8};
        double result =5;
        matrix = new DiagMatrix(matrixd);
        matrix.setElement(2,2,5);
        assertEquals(matrix.getElement(2,2), result);
    }
}
