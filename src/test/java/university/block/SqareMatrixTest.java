package university.block;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class SqareMatrixTest {
    IMatrix matrix;
    @Test
    public void testSimple() throws MatrixException {
        double[] matrixd = {1,0,0,0,4,0,0,0,8};
        double result =32;
        matrix = new SquareMatrix(matrixd);
        assertEquals(matrix.solveMatrix(), result);
    }
    @Test
    public void testSimpleTwo() throws MatrixException {
        double[] matrixd = {2,5,4,-2,4,3,1,0,-2};
        double result =-37;
        matrix = new SquareMatrix(matrixd);
        assertEquals(matrix.solveMatrix(), result);
    }
    @Test
    public void testSimpleThree() throws MatrixException {
        double[] matrixd = {1,2,3,4,5,6,7,8,8};
        double result =3;
        matrix = new SquareMatrix(matrixd);
        assertEquals(matrix.solveMatrix(), result);
    }
    @Test
    public void testZero()throws MatrixException{
        double[] matrixd = {1,2,3,4,5,6,7,8,8};
        double result =11;
        matrix = new SquareMatrix(matrixd);
        matrix.setElement(0,0,0);
        assertEquals(matrix.solveMatrix(), result);


    }
    @Test
    public void testSet() throws MatrixException {
        double[] matrixd = {1,0,0,0,4,0,0,0,8};
        double result =5;
        matrix = new SquareMatrix(matrixd);
        matrix.setElement(2,2,5);
        assertEquals(matrix.getElement(2,2), result);
    }
}
